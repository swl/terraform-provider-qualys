.PHONY: default
default: build

.PHONY: build
build: 
	mkdir -p ${HOME}/.terraform.d/plugins
	go build -o ${HOME}/.terraform.d/plugins/terraform-provider-qualys
	