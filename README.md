[[_TOC_]]

# Terraform Provider for Qualys CloudView

The [Qualys CloudView](https://www.qualys.com/docs/release-notes/qualys-cloudview-18-api-release-notes.pdf) provider is used to provision Qualys CloudView connectors that are used to inventory and monitor workload and infrastructure in supported cloud vendors.

The provider needs to be configured with Qualys CloudView API credential before it can be used. See [Example Usage](#example-usage).

Currently [Stanford GCP project connectors](https://code.stanford.edu/et-ts/qualys-cloud-view)) are provisioned by this provider.

# Maintainer

Xu Wang `<xuwang@stanford.edu>`

# Qualys CloudView Connector Coverage

## GCP Connector API

- [ ] data.qualys_gcp_connector_list
- [x] data.qualys_gcp_connector
- [x] resource.qualys_gcp_connector

## AWS Connector API

- [ ] data.qualys_aws_connector_list
- [ ] data.qualys_aws_connector
- [ ] resource.qualys_aws_connector

##  Azure Connector API
- [ ] data.qualys_azure_connector_list
- [ ] data.qualys_azure_connector
- [ ] resource.qualys_azure_connector

# Requirements

- [Terraform](https://www.terraform.io/downloads.html) 0.12.x
- [Go](https://golang.org/doc/install) 1.11+ (to build the provider plugin)
- Qualys CloudView API credential (username:password)

# Build and Install the Provider

```console
# Set GOPATH, if not already defined:
$ export GOPATH=${HOME}/go  

# Compile the plugin and install to the terraform plugins dir:

$ REPO=code.stanford.edu/xuwang/terraform-provider-qualys
$ git clone https://${REPO} ${GOPATH}/src/${REPO}
$ cd ${GOPATH}/src/${REPO}
$ make build
mkdir -p /Users/xuwang/.terraform.d/plugins
go build -o /Users/xuwang/.terraform.d/plugins/terraform-provider-qualys
```

Check the plugin:

```console
$ ${HOME}/.terraform.d/plugins/terraform-provider-qualys

This binary is a plugin. These are not meant to be executed directly.
Please execute the program that consumes these plugins, which will
load any plugins automatically
```

# Example Usage

* Setup EnvVars

Ask your Qulays CloudView administrator for CloudView API account, and API endpoint url, then set
the following environment(replace url with yours):

```console
export QUALYS_URL=https://qualysguard.qg2.apps.qualys.com
export QUALYS_USERNAME=<your qualys login>
export QUALYS_PASSWORD=<your qualys password>
```

* Example Terraform file (e.g. main.tf )

```console
provider "qualys" {
    // recommend to use the environment variables to avoid checking the credentials
    // base_url = "<your qualys platform url>"
    // username = "<your qualys login>"
    // password = "<your qualys password>"
}

data "qualys_gcp_connector"  "my_test_connector" {
    connector_id = "<connector-uuid>"
}

output "my_test_connector_project_id" {
    value = data.qualys_gcp_connector.my_test_connector.project_id
}

resource "qualys_gcp_connector"  "my_dev_connector" {
    name           = "dev_gcp"
    description    = "dev_gcp"
    config_file     = file("./dev_gcp.json")
}
....
```

* Provision

```console
$ terraform init
$ terraform plan
$ terraform apply
...
```

* Import Data

```console
$ terraform import qualys_gcp_connector.my_dev_connector <my_dev_connector connectionId>
...
```

# References

* [Qualys CloudView](https://www.qualys.com/docs/release-notes/qualys-cloudview-18-api-release-notes.pdf)
* [Qualys Cloudview API](https://qualysguard.qg2.apps.qualys.com/cloudview-api/swagger-ui.html#/)
* [Qualys CloudView Console](https://qualysguard.qg2.apps.qualys.com/portal-front/home/)
* [Provider GCP resource usage](https://code.stanford.edu/et-ts/qualys-cloud-view)