package aws

import (
	"errors"
	"fmt"

	resty "github.com/go-resty/resty/v2"
	"github.com/google/go-querystring/query"
)

const (
	apiPath = "/cloudview-api/rest/v1/aws/connectors"
)

// ConnectorService supports /cloudview-api/rest/v1/aws/connectors
type ConnectorService struct {
	// resty http client
	client *resty.Client
}

// Connector represents a qualys cloudview aws connector
type Connector struct {
	Arn						string `json:"arn"`
	AWSAccountID			string `json:"awsAccountId"`
	BaseAccountID			string `json:"baseAccountId"`
	ConnectorID				string `json:"connectorId"`
	Description				string `json:"description"`
	ExternalID				string `json:"externalId"`
	Groups					[]struct {
		Name string `json:"name"`
		UUID string `json:"uuid"`
	} `json:"groups"`
	IsChinaRegion			bool `json:"isChinaRegion"`
	IsGovCloud				bool `json:"isGovCloud"`
	IsPortalConnector		bool `json:"isPortalConnector"`
	LastSyncedOn			string `json:"lastSyncedOn"`
	Name					string `json:"name"`
	PortalConnectorUUID		string `json:"portalConnectorUuid"`
	Provider				string `json:"provider"`
	State					string `json:"state"`
	ServiceError			string `json:"error`
	TotalAssets				int `json:"totalAssets"`

	//Error response
	Timestamp				string `json:"timestamp"`
	Status					int    `json:"status"`
	ServiceEerror			string `json:"error"`
	ErrorCode				string `json:"errorCode"`
	Message					string `json:"message"`
	Path					string `json:"path"`
}

// Pageable represents pageable info of the api call
type Pageable struct {
	Offset     int  `json:"offset"`
	PageNumber int  `json:"pageNumber"`
	PageSize   int  `json:"pageSize"`
	Paged      bool `json:"paged"`
	Sort       struct {
		Sorted   bool `json:"sorted"`
		Unsorted bool `json:"unsorted"`
	} `json:"sort"`
	UnPaged     bool `json:"unpaged"`
}

// ConnectorList represents a list of aws connectors
type ConnectorList struct {
	List     []Connector `json:"content`
	Pageable *Pageable   `json:"pagable"`
	IsFirst  bool        `json:"first"`
	IsLast   bool        `json:"last"`
	Number   int         `json:"number"`
	Total    int         `json:"numberOfElements"`

	//Error response
	Timestamp		string `json:"timestamp"`
	Status			int    `json:"status"`
	ServiceEerror	string `json:"error"`
	ErrorCode		string `json:"errorCode"`
	Message			string `json:"message"`
	Path			string `json:"path"`
}

// NewService returns a new AWS ConnectorService
func NewService(baseURL, username, password string) *ConnectorService {

	client := resty.New().
		SetHostURL(baseURL).
		SetBasicAuth(username, password).
		//for very buggy api
		SetRetryCount(3)

	return &ConnectorService{client: client}
}

// Get gets a connector by id.
func (s *ConnectorService) Get(id string) (*Connector, error) {

	path := fmt.Sprintf("%s/%s", apiPath, id)
	connector := new(Connector)
	resp, err := s.client.R().
		SetResult(&connector).
		Get(path)

	if err != nil {
		return nil, err
	}

	if resp.StatusCode() >= 400 {
		return nil, errors.New(string(resp.Body()))
	}
	return connector, err
}

// CreateOptions options for create a aws connector
type CreateOptions struct {
	Arn				string `url:"arn" json:"arn"`
	Description		string `url:"description,omitempty" json:"description,omitempty"`
	ExternalID		string `url:"externalId" json:"externalId"`
	IsChinaRegion	bool   `url:"isChinaRegion" json:"isChinaRegion"`
	IsGovCloud		bool   `url:"isGovCloud" json:"isGovCloud"`
	IsPortalConnector bool `url:"isPortalConnector" json:"isPortalConnector"`
	Name			string `url:"name" json:"name"`
}

// Create creates a aws connector.
func (s *ConnectorService) Create(opt *UpdataOptions) (*Connector, error) {

	connector := new(Connector)
	v, err := query.Values(opt)
	if err != nil {
		return nil, err
	}
	req := s.client.R().
		SetResult(&connector).
		SetFormDataFromValues(v)

	resp, err := req.Post(apiPath)

	if err != nil {
		return nil, err
	}

	if resp.StatusCode() >= 400 {
		return nil, errors.New(string(resp.Body()))
	}
	return connector, err
}

// UpdataOptions options for update a aws connector
type UpdataOptions struct {
	Arn				string `url:"arn,omitempty" json:"arn,omitempty"`
	Description		string `url:"description,omitempty" json:"description,omitempty"`
	ExternalID		string `url:"externalId,omitempty" json:"externalId,omitempty"`
	IsChinaRegion	bool   `url:"isChinaRegion,omitempty" json:"isChinaRegion,omitempty"`
	IsGovCloud		bool   `url:"isGovCloud,omitempty" json:"isGovCloud,omitempty"`
	IsPortalConnector bool `url:"isPortalConnector,omitempty" json:"isPortalConnector,omitempty"`
	Name			string `url:"name,omitempty" json:"name,omitempty"`
}

// Update updates a aws connector.
func (s *ConnectorService) Update(id string, opt *UpdataOptions) error {

	path := fmt.Sprintf("%s/%s", apiPath, id)

	v, err := query.Values(opt)
	if err != nil {
		return err
	}
	req := s.client.R().SetFormDataFromValues(v)

	resp, err := req.Put(path)
	if err != nil {
		return err
	}

	if resp.StatusCode() >= 400 {
		return errors.New(string(resp.Body()))
	}
	return nil
}

// DeleteOptions options for delete aws connectors
type DeleteOptions struct {
	ConnectorIds []string `url:"connectorIds,omitempty" json:"connectorIds,omitempty"`
}

// Delete deletes connectors by ids.
func (s *ConnectorService) Delete(opt *DeleteOptions) error {

	resp, err := s.client.R().
		SetBody(opt.ConnectorIds).
		SetHeader("Content-Type", "application/json").
		Delete(apiPath)

	if err != nil {
		return err
	}

	if resp.StatusCode() >= 400 {
		return fmt.Errorf("Delete failed, status code is %d", resp.StatusCode())
	}
	return nil
}
