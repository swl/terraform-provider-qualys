package gcp

import (
	"errors"
	"fmt"
	"strings"

	resty "github.com/go-resty/resty/v2"
	"github.com/google/go-querystring/query"
)

const (
	apiPath = "/cloudview-api/rest/v1/gcp/connectors"
)

// ConnectorService supports /cloudview-api/rest/v1/gcp/connectors
type ConnectorService struct {
	// resty http client
	client *resty.Client
}

// Connector represents a qualys cloudview gcp connector
type Connector struct {
	ConnectorID string `json:"connectorId"`
	Description string `json:"description"`
	Groups      []struct {
		Name string `json:"name"`
		UUID string `json:"uuid"`
	} `json:"groups"`
	LastSyncedOn string `json:"lastSyncedOn"`
	Name        string `json:"name"`
	Project     string `json:"projectId"`
	Provider string `json:"provider"`
	State string `json:"state"`
	TotalAssets int `json:"totalAssets"`

	//Error response
	Timestamp				string `json:"timestamp"`
	Status					int    `json:"status"`
	ServiceEerror			string `json:"error"`
	ErrorCode				string `json:"errorCode"`
	Message					string `json:"message"`
	Path					string `json:"path"`
}

// Pageable represents pageable info of the api call
type Pageable struct {
	Offset     int  `json:"offset"`
	PageNumber int  `json:"pageNumber"`
	PageSize   int  `json:"pageSize"`
	Paged     bool `json:"paged"`
	Sort       struct {
		Sorted   bool `json:"sorted"`
		Unsorted bool `json:"unsorted"`
	} `json:"sort"`
	UnPaged     bool `json:"unpaged"`
}

// ConnectorList represents a list of gcp connectors
type ConnectorList struct {
	List     []Connector `json:"content`
	Pageable *Pageable   `json:"pagable"`
	IsFirst  bool        `json:"first"`
	IsLast   bool        `json:"last"`
	Number   int         `json:"number"`
	Total    int         `json:"numberOfElements"`

	//Error response
	Timestamp				string `json:"timestamp"`
	Status					int    `json:"status"`
	ServiceEerror			string `json:"error"`
	ErrorCode				string `json:"errorCode"`
	Message					string `json:"message"`
	Path					string `json:"path"`
}

// NewService returns a new GCP ConnectorService
func NewService(baseURL, username, password string) *ConnectorService {

	client := resty.New().
		SetHostURL(baseURL).
		SetBasicAuth(username, password).
		//for very buggy api
		SetRetryCount(3)

	return &ConnectorService{client: client}
}

// Get gets a connector by id.
func (s *ConnectorService) Get(id string) (*Connector, error) {

	path := fmt.Sprintf("%s/%s", apiPath, id)
	connector := new(Connector)
	resp, err := s.client.R().
		SetResult(&connector).
		Get(path)

	if err != nil {
		return nil, err
	}

	if resp.StatusCode() >= 400 {
		return nil, errors.New(string(resp.Body()))
	}
	if len(connector.ServiceEerror) > 0 {
		return nil, errors.New(string(resp.Body()))
	}
	if len(connector.ErrorCode) > 0 {
		return nil, errors.New(string(resp.Body()))
	}

	return connector, err
}

// CreateOptions options for create a gcp connector
type CreateOptions struct {
	Name        string `url:"name" json:"name"`
	Description string `url:"description,omitempty" json:"description,omitempty"`
	ConfigFile  string `url:"-" json:"-"`
}

// Create creates a gcp connector.
func (s *ConnectorService) Create(opt *UpdataOptions) (*Connector, error) {

	connector := new(Connector)
	v, err := query.Values(opt)
	if err != nil {
		return nil, err
	}
	req := s.client.R().
		SetResult(&connector).
		SetFormDataFromValues(v)

	if opt.ConfigFile == "" {
		return nil, errors.New("Missing config file")
	} else {
		req.SetFileReader("configFile", "configFile", strings.NewReader(opt.ConfigFile))
	}

	resp, err := req.Post(apiPath)

	if err != nil {
		return nil, err
	}

	if resp.StatusCode() >= 400 {
		return nil, errors.New(string(resp.Body()))
	}
	if len(connector.ServiceEerror) > 0 {
		return nil, errors.New(string(resp.Body()))
	}
	if len(connector.ErrorCode) > 0 {
		return nil, errors.New(string(resp.Body()))
	}
	
	return connector, err
}

// UpdataOptions options for update a gcp connector
type UpdataOptions struct {
	Name        string `url:"name,omitempty" json:"name,omitempty"`
	Description string `url:"description,omitempty" json:"description,omitempty"`
	ConfigFile  string `url:"-" json:"-"`
}

// Update updates a gcp connector.
func (s *ConnectorService) Update(id string, opt *UpdataOptions) error {

	path := fmt.Sprintf("%s/%s", apiPath, id)

	connector := new(Connector)
	v, err := query.Values(opt)
	if err != nil {
		return err
	}
	req := s.client.R().
		SetResult(&connector).
		SetFormDataFromValues(v)

	if opt.ConfigFile == "" {
		// needed for work around a bug that prevent update string-with-space values.
		req.SetFileReader("dumy", "dumy", strings.NewReader("dumy"))
	} else {
		req.SetFileReader("configFile", "configFile", strings.NewReader(opt.ConfigFile))
	}

	resp, err := req.Put(path)
	if err != nil {
		return err
	}

	if resp.StatusCode() >= 400 {
		return errors.New(string(resp.Body()))
	}
	if len(connector.ServiceEerror) > 0 {
		return errors.New(string(resp.Body()))
	}
	if len(connector.ErrorCode) > 0 {
		return errors.New(string(resp.Body()))
	}
	return nil
}

// DeleteOptions options for delete gcp connectors
type DeleteOptions struct {
	ConnectorIds []string `url:"connectorIds,omitempty" json:"connectorIds,omitempty"`
}

// Delete deletes connectors by ids.
func (s *ConnectorService) Delete(opt *DeleteOptions) error {

	resp, err := s.client.R().
		SetBody(opt.ConnectorIds).
		SetHeader("Content-Type", "application/json").
		Delete(apiPath)

	if err != nil {
		return err
	}

	if resp.StatusCode() >= 400 {
		return fmt.Errorf("Delete failed, status code is %d", resp.StatusCode())
	}
	return nil
}
