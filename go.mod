module code.stanford.edu/xuwang/terraform-provider-qualys

go 1.13

require (
	github.com/go-resty/resty/v2 v2.2.0
	github.com/google/go-querystring v1.0.0
	github.com/hashicorp/hcl/v2 v2.3.0 // indirect
	github.com/hashicorp/terraform-plugin-sdk v1.8.0
)
