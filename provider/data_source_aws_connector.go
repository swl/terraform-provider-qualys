package provider

import (
	"log"

	aws "code.stanford.edu/xuwang/terraform-provider-qualys/cloudview/aws"
	"github.com/hashicorp/terraform-plugin-sdk/helper/schema"
)

func dataSourceAWSConnector() *schema.Resource {
	return &schema.Resource{
		Read: dataSourceAWSConnectorRead,

		Schema: map[string]*schema.Schema{
			"arn": {
				Type:     schema.TypeString,
				Required: true,
			},
			"aws_account_id": {
				Type:     schema.TypeString,
				Computed: true,
			},
			"base_account_id": {
				Type:     schema.TypeString,
				Computed: true,
			},
			"connector_id": {
				Type:     schema.TypeString,
				Required: true,
			},
			"external_id": {
				Type:     schema.TypeString,
				Required: true,
			},
			"description": {
				Type:     schema.TypeString,
				Computed: true,
			},
			"cloud_provider": {
				Type:     schema.TypeString,
				Computed: true,
			},
			"groups": {
				Type:     schema.TypeSet,
				Computed: true,
				Elem: &schema.Resource{
					Schema: map[string]*schema.Schema{
						"name": {
							Type:     schema.TypeString,
							Computed: true,
						},
						"uuid": {
							Type:     schema.TypeString,
							Computed: true,
						},
					},
				},
			},
			"is_china_region": {
				Type:     schema.TypeBool,
				Required: true,
			},
			"is_gov_cloud": {
				Type:     schema.TypeBool,
				Required: true,
			},
			"is_portal_connector": {
				Type:     schema.TypeBool,
				Required: true,
			},
			"last_synced_on": {
				Type:     schema.TypeString,
				Computed: true,
			},
			"portal_connector_uuid": {
				Type:     schema.TypeString,
				Computed: true,
			},
			"name": {
				Type:     schema.TypeString,
				Computed: true,
			},
			"total_assets": {
				Type:     schema.TypeInt,
				Computed: true,
			},
			"state": {
				Type:     schema.TypeString,
				Computed: true,
			},
		},
	}
}

func dataSourceAWSConnectorRead(d *schema.ResourceData, meta interface{}) error {

	log.Printf("[DEBUG] Reading aws connector %q ", d.Get("connector_id"))

	service := meta.(*aws.ConnectorService)
	connector, err := service.Get(d.Get("connector_id").(string))
	if err != nil {
		return err
	}
	d.Set("arn", connector.Arn)
	d.Set("aws_account_id", connector.AWSAccountID)
	d.Set("base_account_id", connector.BaseAccountID)
	d.Set("cloud_provider", connector.Provider)
	d.Set("description", connector.Description)
	d.Set("external_id", connector.ExternalID)
	d.Set("groups", connector.Groups)
	d.Set("is_china_region", connector.IsChinaRegion)
	d.Set("is_gov_cloud", connector.IsGovCloud)
	d.Set("is_portal_connector", connector.IsPortalConnector)
	d.Set("last_synced_on", connector.LastSyncedOn)
	d.Set("portal_connector_uuid", connector.PortalConnectorUUID)
	d.Set("name", connector.Name)
	d.Set("state", connector.State)
	d.Set("total_assets", connector.TotalAssets)

	d.SetId(connector.ConnectorID)

	return nil
}
