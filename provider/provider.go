package provider

import (
	gcp "code.stanford.edu/xuwang/terraform-provider-qualys/cloudview/gcp"
	"github.com/hashicorp/terraform-plugin-sdk/helper/schema"
	"github.com/hashicorp/terraform-plugin-sdk/terraform"
)

// Provider returns a terraform.ResourceProvider.
func Provider() terraform.ResourceProvider {

	// The actual provider
	return &schema.Provider{
		Schema: map[string]*schema.Schema{
			"username": {
				Type:        schema.TypeString,
				Required:    true,
				DefaultFunc: schema.EnvDefaultFunc("QUALYS_USERNAME", ""),
				Description: "The BasicAuth username to connect to Qualys API.",
			},
			"password": {
				Type:        schema.TypeString,
				Required:    true,
				DefaultFunc: schema.EnvDefaultFunc("QUALYS_PASSWORD", ""),
				Description: "The BasicAuth password to connect to Qualys API.",
			},
			"base_url": {
				Type:        schema.TypeString,
				Optional:    true,
				DefaultFunc: schema.EnvDefaultFunc("QUALYS_URL", ""),
				Description: "The Qualys Base API URL",
			},
		},

		DataSourcesMap: map[string]*schema.Resource{
			"qualys_gcp_connector": dataSourceGCPConnector(),
			"qualys_aws_connector":	dataSourceAWSConnector(),
			// "qualys_azure_connector":	dataSourceAzureConnector(),
			// "qualys_aws_connector":	dataSourceAWSConnector(),
			// "qualys_connector_group":	dataSoruceConnectorGroup(),
		},

		ResourcesMap: map[string]*schema.Resource{
			"qualys_gcp_connector": resourceGCPConnector(),
			// "qualys_azure_connector":	resourceAzureConnector(),
			// "qualys_aws_connector":	resourceAWSConnector(),
			// "qualys_connector_group":	resourceConnectorGroup(),
		},

		ConfigureFunc: providerConfigure,
	}
}

var descriptions map[string]string

func providerConfigure(d *schema.ResourceData) (interface{}, error) {
	username := d.Get("username").(string)
	password := d.Get("password").(string)
	baseURL := d.Get("base_url").(string)
	return gcp.NewService(baseURL, username, password), nil
}
