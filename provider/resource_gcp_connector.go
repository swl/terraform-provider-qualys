package provider

import (
	"log"

	gcp "code.stanford.edu/xuwang/terraform-provider-qualys/cloudview/gcp"
	"github.com/hashicorp/terraform-plugin-sdk/helper/schema"
)

func resourceGCPConnector() *schema.Resource {
	return &schema.Resource{
		Create: resourceGCPConnectorCreate,
		Read:   resourceGCPConnectorRead,
		Update: resourceGCPConnectorUpdate,
		Delete: resourceGCPConnectorDelete,
		Importer: &schema.ResourceImporter{
			State: schema.ImportStatePassthrough,
		},

		Schema: map[string]*schema.Schema{
			"connector_id": {
				Type:     schema.TypeString,
				Computed: true,
			},
			"name": {
				Type:     schema.TypeString,
				Required: true,
			},
			"description": {
				Type:     schema.TypeString,
				Optional: true,
			},
			"config_file": {
				Type:      schema.TypeString,
				Sensitive: true,
				Required:  true,
			},
			"project_id": {
				Type:     schema.TypeString,
				Computed: true,
			},
			"cloud_provider": {
				Type:     schema.TypeString,
				Computed: true,
			},
		},
	}
}

func resourceGCPConnectorSetToState(d *schema.ResourceData, connector *gcp.Connector) {
	d.Set("connector_id", connector.ConnectorID)
	d.Set("name", connector.Name)
	d.Set("description", connector.Description)
	d.Set("groups", connector.Groups)
	d.Set("project_id", connector.Project)
	_, ok := d.GetOk("config_file")
	if !ok {
		d.Set("config_file", "")
	}
}

func resourceGCPConnectorCreate(d *schema.ResourceData, meta interface{}) error {

	log.Printf("[DEBUG] create connector %q", d.Get("name").(string))

	opt := &gcp.UpdataOptions{}
	opt.Name = d.Get("name").(string)
	opt.Description = d.Get("description").(string)
	opt.ConfigFile = d.Get("config_file").(string)

	service := meta.(*gcp.ConnectorService)
	connector, err := service.Create(opt)

	if err != nil {
		return err
	}

	d.SetId(connector.ConnectorID)
	return resourceGCPConnectorRead(d, meta)
}

func resourceGCPConnectorRead(d *schema.ResourceData, meta interface{}) error {

	log.Printf("[DEBUG] Reading gcp connector %q ", d.Id())

	service := meta.(*gcp.ConnectorService)
	connector, err := service.Get(d.Id())
	if err != nil {
		d.SetId("")
		return err
	}
	resourceGCPConnectorSetToState(d, connector)

	return nil
}

func resourceGCPConnectorUpdate(d *schema.ResourceData, meta interface{}) error {

	log.Printf("[DEBUG] update gcp connector %s", d.Id())
	opt := &gcp.UpdataOptions{}

	if d.HasChange("name") {
		opt.Name = d.Get("name").(string)
	}

	if d.HasChange("description") {
		opt.Description = d.Get("description").(string)
	}

	if d.HasChange("config_file") {
		opt.ConfigFile = d.Get("config_file").(string)
	}

	service := meta.(*gcp.ConnectorService)
	err := service.Update(d.Id(), opt)
	if err != nil {
		return err
	}
	return resourceGCPConnectorRead(d, meta)
}

func resourceGCPConnectorDelete(d *schema.ResourceData, meta interface{}) error {

	log.Printf("[DEBUG] Delete qualys connector %s", d.Id())
	opt := &gcp.DeleteOptions{}

	opt.ConnectorIds = make([]string, 1, 1)
	opt.ConnectorIds[0] = d.Id()

	service := meta.(*gcp.ConnectorService)
	err := service.Delete(opt)
	if err != nil {
		return err
	}
	return nil
}
